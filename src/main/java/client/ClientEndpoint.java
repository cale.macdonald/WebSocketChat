package client;

import messaging.*;
import messaging.text.ChatMessage;

import javax.websocket.OnMessage;

@javax.websocket.ClientEndpoint(encoders = MessageEncoder.class, decoders = MessageDecoder.class)
public class ClientEndpoint {
    @OnMessage
    public void onMessage(Message message) {
        if (message instanceof ChatMessage) {
            System.out.println(message.toString());
        }
    }
}
