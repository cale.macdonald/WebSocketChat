package client;

import messaging.Message;
import org.glassfish.tyrus.client.ClientManager;

import javax.websocket.Session;
import java.net.URI;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws Exception {
        String userName = promptForUserName();
        ClientManager manager = ClientManager.createClient();

        URI endpoint = new URI("ws://localhost:8888/ws/chat");
        Session session = manager.connectToServer(ClientEndpoint.class, endpoint);

        Scanner scanner = new Scanner(System.in);
        String inputText;
        do {
            inputText = scanner.nextLine();

            Message message = Message.create(inputText, userName);
            session.getBasicRemote().sendObject(message);
        } while (!inputText.toLowerCase().contains("quit"));
    }

    private static String promptForUserName() {
        System.out.print("Enter username: ");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }
}
