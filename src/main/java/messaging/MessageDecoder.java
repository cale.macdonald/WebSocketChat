package messaging;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import util.MessageJsonAdapter;

import javax.websocket.*;

public class MessageDecoder implements Decoder.Text<Message> {
    private static Gson serializer;

    static {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Message.class, new MessageJsonAdapter());
        serializer = builder.create();
    }

    @Override
    public Message decode(String text) {
        return serializer.fromJson(text, Message.class);
    }

    @Override
    public boolean willDecode(String s) {
        return true;
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
