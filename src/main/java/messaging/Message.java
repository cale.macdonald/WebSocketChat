package messaging;

import messaging.text.ChatMessage;

import java.time.Instant;
import java.util.Date;

public abstract class Message {
    private final String issuer;
    private final Date timestamp;

    protected Message(String issuer) {
        this.issuer = issuer;
        this.timestamp = Date.from(Instant.now());
    }

    public String getIssuer() {
        return this.issuer;
    }

    protected Date getTimestamp() {
        return this.timestamp;
    }

    public static Message create(String text, String issuer) {
        if (text.startsWith("/")) {
            return Command.FromText(text, issuer);
        }
        return new ChatMessage(issuer, text);
    }
}
