package messaging;

import messaging.commands.PrivateMessageCommand;
import messaging.commands.QuitCommand;
import messaging.commands.UnknownCommand;
import server.ServerEndpoint;

import javax.websocket.Session;

public abstract class Command extends Message {
    protected Command(String issuer) {
        super(issuer);
    }

    public static Command FromText(String commandText, String issuer) {
        if (commandText.equalsIgnoreCase("/QUIT")) {
            return new QuitCommand(issuer);
        }

        if (commandText.toLowerCase().contains("/msg")) {
            String[] commandParts = commandText.split(" ", 3);
            String userId = commandParts[1];
            String message = commandParts[2];
            return new PrivateMessageCommand(issuer, userId, message);
        }

        return new UnknownCommand(issuer);
    }

    public abstract void execute(ServerEndpoint server, Session session) throws Exception;
}
