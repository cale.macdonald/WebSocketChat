package messaging.text;

import messaging.Message;

public class ChatMessage extends Message {
    private final String content;

    public ChatMessage(String issuer, String content) {
        super(issuer);

        this.content = content;
    }

    @Override
    public String toString() {
        return String.format("[%s] %s: %s", super.getTimestamp(), super.getIssuer(), this.content);
    }
}
