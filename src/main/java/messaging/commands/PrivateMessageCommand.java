package messaging.commands;

import messaging.Command;
import messaging.text.ChatMessage;
import server.Connection;
import server.ServerEndpoint;

import javax.websocket.Session;
import java.util.Optional;
import java.util.Set;

public class PrivateMessageCommand extends Command {
    private final String userId;
    private final String message;

    public PrivateMessageCommand(String issuer, String userId, String message) {
        super(issuer);
        this.userId = userId;
        this.message = message;
    }

    @Override
    public void execute(ServerEndpoint server, Session session) {
        Optional<Connection> user = this.findUser(server.getConnections());
        user.ifPresent(connection -> connection.broadcast(new ChatMessage(super.getIssuer(), this.message)));
    }

    private Optional<Connection> findUser(Set<Connection> sessions) {
        return sessions.stream().filter(s -> s.getId().equalsIgnoreCase(this.userId)).findFirst();
    }
}
