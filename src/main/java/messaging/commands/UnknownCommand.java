package messaging.commands;

import messaging.Command;
import server.ServerEndpoint;

import javax.websocket.Session;

public class UnknownCommand extends Command {
    public UnknownCommand(String issuer) {
        super(issuer);
    }

    @Override
    public void execute(ServerEndpoint server, Session session) {
    }
}
