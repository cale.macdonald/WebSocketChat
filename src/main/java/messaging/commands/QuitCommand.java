package messaging.commands;

import messaging.Command;
import server.ServerEndpoint;

import javax.websocket.Session;

public class QuitCommand extends Command {

    public QuitCommand(String issuer) {
        super(issuer);
    }

    @Override
    public void execute(ServerEndpoint server, Session session) throws Exception {
        session.close();
    }
}
