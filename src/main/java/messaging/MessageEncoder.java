package messaging;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import util.MessageJsonAdapter;

import javax.websocket.Encoder;
import javax.websocket.EndpointConfig;

public class MessageEncoder implements Encoder.Text<Message> {
    private static Gson serializer;

    static {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(Message.class, new MessageJsonAdapter());
        serializer = builder.create();
    }

    @Override
    public String encode(Message message) {
        return serializer.toJson(message, Message.class);
    }

    @Override
    public void init(EndpointConfig config) {
    }

    @Override
    public void destroy() {
    }
}
