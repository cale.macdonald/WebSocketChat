package server;

import javax.websocket.DeploymentException;
import java.util.Scanner;

public class Server {
    public static void main(String[] args) {
        final String host = "localhost";
        final int port = 8888;
        final String rootPath = "/ws";

        org.glassfish.tyrus.server.Server server = new org.glassfish.tyrus.server.Server(
                host, port, rootPath, ServerEndpoint.class);

        boolean serverStarted = false;
        try {
            server.start();
            serverStarted = true;
            System.out.print("Press any key to continue...");
            new Scanner(System.in).nextLine();
        } catch (DeploymentException e) {
            e.printStackTrace();
        } finally {
            if (serverStarted) {
                server.stop();
            }
        }
    }
}
