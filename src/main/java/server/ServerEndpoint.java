package server;

import messaging.*;
import messaging.commands.PrivateMessageCommand;
import messaging.commands.QuitCommand;
import messaging.text.ChatMessage;

import javax.websocket.*;
import java.io.IOException;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@javax.websocket.server.ServerEndpoint(
        value = "/chat",
        encoders = { MessageEncoder.class },
        decoders = { MessageDecoder.class }
)
public class ServerEndpoint {
    private static Set<Connection> connections = Collections.synchronizedSet(new HashSet<>());

    public Set<Connection> getConnections() {
        return Collections.unmodifiableSet(connections);
    }

    @OnOpen
    public void onOpen(Session session) {
        System.out.println(String.format("Connection created. Session id %s", session.getId()));
        connections.add(new Connection(session));
    }

    @OnClose
    public void onClose(Session session) {
        System.out.println(String.format("Ending session %s", session.getId()));
        connections.removeAll(
                connections.stream().filter(con -> con.getId().equals(session.getId())).
                        collect(Collectors.toList()));
    }

    @OnMessage
    public void onMessage(Message message, Session session) {
        if (message instanceof Command) {
            try {
                ((Command)message).execute(this, session);
            } catch (Exception e) {
                System.err.println("Error executing command");
            }

            return;
        }

        broadcast(message);
    }

    private void broadcast(Message message) {
        for (Connection connection : connections) {
            connection.broadcast(message);
        }
    }
}
