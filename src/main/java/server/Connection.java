package server;

import messaging.Message;

import javax.websocket.EncodeException;
import javax.websocket.Session;
import java.io.IOException;

public class Connection {
    private final Session session;

    Connection(Session session) {
        this.session = session;
    }

    public String getId() {
        return session.getId();
    }

    public void broadcast(Message message) {
        try {
            this.session.getBasicRemote().sendObject(message);
        } catch (IOException | EncodeException e) {
            e.printStackTrace();
        }
    }
}
